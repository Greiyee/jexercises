
import java.util.*;


public class ArrayCollectionsExercise {

    public static void main (String [] args){

String[] countries= {"USA","Korea","Japan","Ireland","United Kingdom","Spain",
                    "Australia","China","France","Switzerland"};

    System.out.println("The size of country is: "+countries.length);
    System.out.println(" ");


    List<String> l1  = new ArrayList<>(Arrays.asList(countries)); 
        System.out.println("The size of country is: "+l1);
        System.out.println(" ");
//
l1.add("Scotland" );
//
System.out.println("The size of country is: "+l1);
System.out.println(" ");
System.out.println("The size of country is: "+l1.size());
System.out.println(" ");

//If Array Empty or Not
    List<String> countrylist  = new ArrayList<>(Arrays.asList(countries)); 
        if (countrylist.isEmpty())         
  {  
            System.out.println("The ArrayList is empty");  
        }  
        else  
            {  
            System.out.println("The ArrayList is not empty"); 
            System.out.println(" ");

//If Japan is in the Array 
    if(l1.contains("Scotland")) {
        System.out.println("Scotland is in the List");
        System.out.println(" ");

//If Philippines is in the Array 
    if(!l1.contains("Mongolia")) {
        System.out.println("Mongolia is not in the List");
        System.out.println(" ");

//Remove Scotland 
    l1.remove(10);
        System.out.println("Scotland is Removed, The size of country is now: "+l1.size());
        System.out.println(" ");

//Top 5 Countries
    if (l1.contains("Ireland"))
        System.out.println("Ireland is in the list");
    if (l1.contains("Korea"))
        System.out.println("Korea is in the list");
    if (l1.contains("Japan"))
        System.out.println("Japan is in the list");
    if (l1.contains("USA"))
        System.out.println("USA is in the list");
    if (l1.contains("Indonesia"))
        System.out.println("Indonesia is in the list");
        System.out.println(" ");

//Display countries using sys out
    List<String> countryList  = new ArrayList<>(Arrays.asList(countries)); 
        System.out.println(countryList);
           System.out.println(" ");

//Hash map Ranking
    HashMap<Integer, String> countryMap = new HashMap<Integer, String>();

    countryMap.put(10,"Switzerland");
    countryMap.put(8,"China");
    countryMap.put(6,"Spain");
    countryMap.put(4,"Ireland");
    countryMap.put(2,"Korea");
    countryMap.put(9,"France");
    countryMap.put(7,"Australia");
    countryMap.put(5,"United Kingdom");
    countryMap.put(3,"Japan");
    countryMap.put(1,"USA");
    
        System.out.println("Rank of countries: "+countryMap);
        System.out.println(" ");

//Sorted country in Array list
    Set set = countryMap.entrySet();

//Elements of LinkedHashMap
    Iterator iterator = set.iterator();

//Sorted country in Array list
    Collections.sort(l1);
        LinkedHashSet<String> lhs = new LinkedHashSet<String>();

   /* Adding ArrayList elements to the LinkedHashSet
    * in order to remove the duplicate elements and 
    * to preserve the insertion order.
    */
   lhs.addAll(l1);

   // Removing ArrayList elements
   l1.clear();

   // Adding LinkedHashSet elements to the ArrayList
   l1.addAll(lhs);
   System.out.println("List of countries alphabetically : "+l1);
   System.out.println(" ");

   Set Set = countryMap.entrySet();

   Map<Integer, String> map = new HashMap<Integer, String>();
   
   map.put(10,"Switzerland");
   map.put(8,"China");
   map.put(6,"Spain");
   map.put(4,"Ireland");
   map.put(2,"Korea");
   map.put(9,"France");
   map.put(7,"Australia");
   map.put(5,"United Kingdom");
   map.put(3,"Japan");
   map.put(1,"USA");

   //Loop a Map
     for (int key : map.keySet()) {
         System.out.println("#"+key+" "+map.get(key));
     }
   

   
}
}
}
   }
}
