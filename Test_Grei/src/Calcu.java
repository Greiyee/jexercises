import Implements.Implements;



public class Calcu implements Implements {
    

    public static void main(String[] args) {
        
        Calcu calcu = new Calcu();
        
        System.out.println("Sum: "+ calcu.getSum(100,355));
        System.out.println("Difference: "+ calcu.getDifference(200,100));
        System.out.println("Product: "+ calcu.getProduct(15,2));
        System.out.println("Quotient and Remainder is:"+ calcu.getQuotientAndRemainder(36, 7));
        System.out.println("Celsius: "+ calcu.toCelsius(40));
        System.out.println("Fahrenheit: "+ calcu.toFahrenheit(100));
        System.out.println("Pound: "+ calcu.toPound(42.3));
        System.out.println("Kilogram: "+ calcu.toKilogram(90));
        System.out.println("Palindrome:" + calcu.isPalindrome("civic"));
        System.out.println("Palindrome:" + calcu.isPalindrome("lodi"));
        System.out.println("Palindrome:" + calcu.isPalindrome("radar_&_radar"));
        
    }

    @Override
    public int getSum(int augend, int addend) {
        // TODO Auto-generated method stub
        return augend+addend;
    }

    @Override
    public double getDifference(double minuend, double subtrahend) {
        // TODO Auto-generated method stub
        return minuend-subtrahend;
    }

    @Override
    public double getProduct(double multiplicand, double multiplier) {
        // TODO Auto-generated method stub
        return multiplicand*multiplier;
    }

    @Override
    public String getQuotientAndRemainder(final int dividend, final int divisor) {
        final int quotient = dividend / divisor;
        final int remainder = dividend % divisor;
        return quotient + " remainder " + remainder;
    }
    
    @Override
    public double toCelsius(int fahrenheit) {
        // TODO Auto-generated method stub             
        return (fahrenheit - 32) * 1.8;
       
    }

    @Override
    public double toFahrenheit(int celsius) {
        // TODO Auto-generated method stub
        return celsius * 1.8 + 32;
    }

    @Override
    public double toKilogram(double lbs) {
        // TODO Auto-generated method stub
        return lbs / 2.2;
    }

    @Override
    public double toPound(double kg) {
        // TODO Auto-generated method stub
        return kg * 2.2;
    }

    @Override
    public boolean isPalindrome(String str) {
        int i;
                final int n = str.length();
                String str1 = "";
         
                for (i = n - 1; i >= 0; i--) {
                    str1 = str1 + str.charAt(i);
                }
        
                if(str1.equals(str)) {
                    return true;
                } else {
                    return false;
        
                }
    }

    }